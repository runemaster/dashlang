"""Parsers available in dashlang to convert from some language to the final
component layout tree.
"""
from typing import Any, List, Mapping, Type

from dash.development.base_component import Component
from parsimonious.grammar import Grammar

from .components import load_component_from_namespace
from .grammars import DASHLANG_GRAMMAR
from .visitor import DashlangVisitor


class _BaseParser:
    """Base class for all parsers. Defines the common `to_layout` method that
    needs to be implemented by every subclass.
    """
    def to_layout(self) -> Component:
        """Convert the result of the parsing stage to the layout tree. The tree
        has a single Component as the root and all other components are nodes in the
        layout tree.
        """
        raise NotImplementedError


class MarkupParser(_BaseParser):
    """Parser class used to parse strings in the dashlang markup format."""

    _content: Mapping[str, Any]

    def __init__(self) -> None:
        self._content = {}

    def parse(self, markup: str) -> None:
        """Parser method that converts a string `markup` containing a component
        layout in markup formant.

        Args:
            markup: The string containing the markup text
        """
        markup = markup.strip().rstrip()

        grammar = Grammar(DASHLANG_GRAMMAR)
        visitor = DashlangVisitor()

        tree = grammar.parse(markup)
        self._content = visitor.visit(tree)

    def to_layout(self) -> Component:
        return self._transform_component_definition(self._content)

    def _transform_component_definition(self, component_definition: Mapping[str, Any]) -> Component:
        """Transform a component definition hold by a dictionary into a loaded
        instance of the component.

        Args:
            component_definition: A mapping that holds the definition for a component.

        Returns:
            The loaded component.
        """
        namespace: str = component_definition["component"]["namespace"]
        name: str = component_definition["component"]["name"]

        component_cls: Type[Component] = load_component_from_namespace(namespace, name)
        component = component_cls(**component_definition["component"]["props"])

        children: List[Component] = [
            self._transform_component_definition(item)
            for item in component_definition["children"]
        ]
        component.children = children if children else None

        return component

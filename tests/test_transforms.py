"""Unit tests for the transforms module"""
import unittest

from dashlang import transforms


class TestStripStringQuotes(unittest.TestCase):
    """Test cases for the `strip_string_quotes` function"""
    def test_valid_string(self) -> None:
        output: str = transforms.strip_string_quotes('"test"')
        self.assertEqual(output, "test")

    def test_no_quotes_string(self) -> None:
        output: str = transforms.strip_string_quotes("test")
        self.assertEqual(output, "test")

    def test_quotes_in_beginning(self) -> None:
        output: str = transforms.strip_string_quotes('"test')
        self.assertEqual(output, "test")

    def test_quotes_in_ending(self) -> None:
        output: str = transforms.strip_string_quotes('test"')
        self.assertEqual(output, "test")

    def test_quotes_in_middle(self) -> None:
        output: str = transforms.strip_string_quotes('te"st')
        self.assertEqual(output, 'te"st')


class TestUnescapeHtmlString(unittest.TestCase):
    """Test cases for the `unescape_html_string` function"""
    def test_escaped_string(self) -> None:
        output: str = transforms.unescape_html_string("1 &lt; 2")
        self.assertEqual(output, "1 < 2")

    def test_unescaped_string(self) -> None:
        output: str = transforms.unescape_html_string("hello there!")
        self.assertEqual(output, "hello there!")

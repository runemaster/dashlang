"""Unit tests for the visitor class"""
import unittest

from dashlang.visitor import DashlangVisitor


class TestDashlangVisitor(unittest.TestCase):
    def setUp(self) -> None:
        self.visitor = DashlangVisitor()

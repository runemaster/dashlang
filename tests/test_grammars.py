"""Unit tests used to validate grammars"""
import unittest

from parsimonious.exceptions import IncompleteParseError, ParseError
from parsimonious.grammar import Grammar

from dashlang.grammars import (
    ASCII_WORD,
    ATTRIBUTE,
    ELEMENT_NAME,
    ELEMENT_NAME_COMPLETE,
    ELEMENT_NAME_SIMPLE,
    FREE_TEXT,
)


class TestAsciiWordRule(unittest.TestCase):
    """Test cases for the ascii_word grammar rule"""
    def setUp(self) -> None:
        self.grammar = Grammar(**{"ascii_word": ASCII_WORD})

    def test_ascii_string(self) -> None:
        tree = self.grammar["ascii_word"].parse("word")
        self.assertEqual(tree.text, "word")

    def test_ascii_numbers(self) -> None:
        tree = self.grammar["ascii_word"].parse("123")
        self.assertEqual(tree.text, "123")

    def test_ascii_numbers_and_letters(self) -> None:
        tree = self.grammar["ascii_word"].parse("word123")
        self.assertEqual(tree.text, "word123")

    def test_underscore_allowed(self) -> None:
        tree = self.grammar["ascii_word"].parse("test_word")
        self.assertEqual(tree.text, "test_word")

    def test_dash_allowed(self) -> None:
        tree = self.grammar["ascii_word"].parse("test-word")
        self.assertEqual(tree.text, "test-word")

    def test_no_spaces_allowed(self) -> None:
        self.assertRaises(
            IncompleteParseError,
            self.grammar["ascii_word"].parse,
            "test word"
        )

    def test_no_marks_allowed(self) -> None:
        self.assertRaises(
            ParseError,
            self.grammar["ascii_word"].parse,
            "!test,word"
        )


class TestFreetextRule(unittest.TestCase):
    """Test cases for the free_text grammar rule"""
    def setUp(self) -> None:
        self.grammar = Grammar(**{"free_text": FREE_TEXT})

    def test_simple_free_text_string(self) -> None:
        tree = self.grammar["free_text"].parse('"hello"')
        self.assertEqual(tree.text, '"hello"')

    def test_string_with_spaces(self) -> None:
        tree = self.grammar["free_text"].parse('"hello world"')
        self.assertEqual(tree.text, '"hello world"')

    def test_string_with_special_characters(self) -> None:
        tree = self.grammar["free_text"].parse('"Hello, world!"')
        self.assertEqual(tree.text, '"Hello, world!"')

    def test_string_with_escaped_content(self) -> None:
        tree = self.grammar["free_text"].parse(
            '"{&#34;key&#34;: &#34;value&#34;, &#34;a&#34;: 123}"'
        )
        self.assertEqual(
            tree.text,
            '"{&#34;key&#34;: &#34;value&#34;, &#34;a&#34;: 123}"'
        )


class TestAttributeRule(unittest.TestCase):
    """Test cases for the attribute sequence rule"""
    def setUp(self) -> None:
        self.grammar = Grammar(**{"attribute": ATTRIBUTE})

    def test_attribute_simple(self) -> None:
        tree = self.grammar["attribute"].parse('attr="value"')
        key, _, value = tree.children
        self.assertEqual(key.text, "attr")
        self.assertEqual(value.text, '"value"')

    def test_attribute_key_with_dash(self) -> None:
        tree = self.grammar["attribute"].parse('attr-name="value"')
        key, _, value = tree.children
        self.assertEqual(key.text, "attr-name")

    def test_attribute_with_long_content(self) -> None:
        tree = self.grammar["attribute"].parse('attr="value is a long string"')
        key, _, value = tree.children
        self.assertEqual(value.text, '"value is a long string"')

    def test_attribute_whitespace_not_allowed(self) -> None:
        self.assertRaises(
            ParseError,
            self.grammar["attribute"].parse,
            'attr = "value"'
        )

    class TestElementNameSimpleRule(unittest.TestCase):
        def setUp(self) -> None:
            self.grammar = Grammar(**{"element_name_simple": ELEMENT_NAME_SIMPLE})

        def test_valid_element_name(self) -> None:
            tree = self.grammar["element_name_simple"].parse("Div")
            self.assertEqual(tree.text, "Div")

    class TestElementNameCompleteRule(unittest.TestCase):
        def setUp(self) -> None:
            self.grammar = Grammar(**{"element_name_complete": ELEMENT_NAME_COMPLETE})

        def test_valid_element_name(self) -> None:
            tree = self.grammar["element_name_complete"].parse("dash_core_components.Graph")
            namespace, _, name = tree.children
            self.assertEqual(namespace, "dash_core_components")
            self.assertEqual(name, "Graph")

    class TestElementNameRule(unittest.TestCase):
        def setUp(self) -> None:
            self.grammar = Grammar(**{"element_name": ELEMENT_NAME})

        def test_element_name_simple(self) -> None:
            tree = self.grammar["element_name"].parse("Graph")
            self.assertEqual(tree.children[0].expr.name, "element_name_simple")
            self.assertEqual(tree.children[0].children[0].text, "Graph")

        def test_element_name_complete(self) -> None:
            tree = self.grammar["element_name"].parse("dash_core_components.Graph")
            namespace, _, name = tree.children[0].children
            self.assertEqual(tree.children[0].expr.name, "element_name_complete")
            self.assertEqual(namespace, "dash_core_components")
            self.assertEqual(name, "Graph")

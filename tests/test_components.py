"""Unit tests for the components module."""
import unittest

import dash_html_components as html
from dashlang.components import load_component_from_namespace


class TestComponents(unittest.TestCase):
    """Test cases for the components module."""
    def test_load_valid_component(self) -> None:
        component_class = load_component_from_namespace("dash_html_components", "Div")
        self.assertEqual(component_class, html.Div)

    def test_load_invalid_namespace(self) -> None:
        self.assertRaises(
            AttributeError,
            load_component_from_namespace,
            "fake_namespace",
            "Div"
        )

    def test_load_missing_component(self) -> None:
        self.assertRaises(
            AttributeError,
            load_component_from_namespace,
            "dash_html_components",
            "fake_component"
        )

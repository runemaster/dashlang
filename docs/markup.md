# Dashlang markup language

This document provides a brief explanation of the markup language that is part
of the Dashlang package. Its goal is to provide an introduction to the language itself,
for both the basic and the more advanced features, and not to provide an explanation
for its technical implementation.

## Goals

The Dashlang markup language was created with two main goals in mind:

* Lateral knowledge transfer: Provide a way for creating Dash application layouts
where users can take advantage of their knowledge of HTML;

* Abstraction: Create a system that abstracts the layout away from the Dash application
and allows the same piece of layout to be reused across implementations;

## Language elements

The markup language is made of multiple elements that can be used to describe a layout
made of a tree of components in a declarative way. Since the language tries to stay as
close to HTML as possible at all times, many of the expressions that make up the language
grammar are close siblings of HTML, and to a lesser extent, XML.

### Elements

The element is the base building block available. Each element will be mapped to a
Dash component class after it has been parsed. As such, the root of every document that
describes a layout will always be a single element, since the root of the layout tree is
by definition a single component.

The following is a simple example of an element as declared using the markup language:

```
<Div id="unique-id"></Div>
```

This snippet declares a single element, named `Div`, that has a single attribute named
`id` with the value `unique-id`. The element declaration is made of two blocks: the element
opening tag and the element closing tag.

#### Element tag

The element opening tag is the tag that contains all of the information used to create the
element, i.e., the element name and its attributes. The opening tag always follows the
convention of starting with the character `<`, followed by the element name, followed by the
element attributes, and finishing with the character `>`.

The element closing tag is a simpler version of the opening tag that is responsible for
indicating to the parser where the element declaration ends. As such, the closing tag does
not allow any attributes to be declared in it. Instead, the only constraint is that the
element name, as declared on the closing tag, must fully match the element name as declared
on the opening tag. The closing tag always follows the convetion of starting with the
characters `</`, followed by the element name, and ending with the character `>`.

#### Element name

Since the element name is what Dashlang uses to retrieve the component class from its
respective package, it must provide a way for the parser to know from which package to
retrieve it. This can be achieved in two ways:

* Namespaces: The element name is made of two separate strings: the `namespace`; and the
`name`. The `namespace` string indicates to Dashlang parser from which package to import
the component with the respecitve `name`. In this case the full element name follows the
form `namespace.name`.

* Default namespace: In cases where the namespace is omitted, the parser will assume that
the component with the name `name` should be imported from the package indicated by the
`default_namespace`. The `default_namespace` can be indicated as an argument to the
`MarkupParser`, and by default its value is `dash_html_components`.

### Attributes

#### String attributes

#### Integer attributes

#### Float attributes

#### Boolean attributes
